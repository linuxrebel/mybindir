#!/bin/bash

for i in `ls`; do
  if [ -d "$i" ]; then
    sed -i 's#https://github.com/#git@github.com:#g' ./$i/.git/config > /dev/null 2>&1
  fi

done
