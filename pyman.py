#!/usr/bin/python

import pydoc
import sys

# Get the object or module to display help for
if len(sys.argv) < 2:
    sys.exit("Usage: python man.py <module or object>")

target = sys.argv[1]

# Capture the help output
h = sys.stdout
sys.stdout = output = sys.stderr = open('help_output.txt', 'w')

# Get the help text
try:
    help(target)
except:
    # If the target is not found, display an error message
    sys.stderr.write(f"No Python documentation found for '{target}'\n")
    sys.exit(1)

# Reset the standard output
sys.stdout = h
sys.stderr = sys.stderr

# Read the captured help output
with open('help_output.txt', 'r') as f:
    help_text = f.read()

# Display the help text with appropriate formatting
formatted_output = pydoc.plain(help_text)
print(formatted_output)
