clear
C:\Users\james\bin\screenfetch.ps1
# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}
Set-Alias -Name vi -Value vim.bat
Set-Alias -Name awk -Value gawk.exe
Set-Alias -Name powershell -Value powershell.exe
function apa {adb shell cmd package list packages}
Set-Alias apa apac
#Set-Alias -Name curl2 -Value C:\ProgramData\chocolatey\lib\curl\tools\curl-7.64.1-win64-mingw\ bin\curl.exe                        

# source our sudo script
#. \Users\james\Documents\WindowsPowerShell\sudo.ps1
#function up {systeminfo | find "System Boot Time"}
function up { PsInfo64.exe | grep.exe "Uptime" | awk -F: '{ print $2 }' | sed -e 's/^[ \t]*//'}
Set-Alias uptime up
function menubkp {Export-StartLayout -UseDesktopApplicationID -Path C:\Users\james\Documents\WindowsPowerShell\Scripts\layout.xml}
Set-Alias mbk menubkp
function cnct {adb.exe connect 127.0.0.1:58526}
Set-Alias wsa-connect cnct


# this function setsup things so I can get all history not just this session                                                      
function gHist {Get-Content C:\Users\james\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt} 
Set-Alias allhist gHist                                                                                                           
Set-Alias ahist gHist                                                                                                             
# function to clear all wsl instances (Ubuntu) so it releases mount usb devices.
function uclr {wsl.exe --shutdown Ubuntu}
Set-Alias uclear uclr

# now we create a function to setup our dynamic proxy through the jumphost
#function sProxy {ssh -s -C -D 7060 jsparenberg@jumphost.tlcinternal.com}
#Set-Alias jstart sProxy
#function aProxy {ssh -C -D 7080 jsparenberg@bastion.lendingcloud.us}
#Set-Alias bstart aProxy

# $PSVersionTable.PSVersion
