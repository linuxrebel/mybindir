#!/usr/bin/python

import os

pAth = input("what dir to traverse\n")

for root, dirs, files in os.walk(pAth, topdown=True):
    for name in files:
        print(os.path.join(root, name))
    for name in dirs:
        print(os.path.join(root, name))
