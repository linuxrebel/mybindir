#!/usr/bin/python


from random import randint
from time import sleep

def dialogue(speaker, *lines):
  print(speaker + ": ")
  print("\n".join(*lines) + "\n")
  sleep(2)

def strange(*lines):
  dialogue("Dr Strange", lines)

def dormammu(*lines):
  dialogue("Dormammu", lines)

def bargain():
  strange("Dormammu! I've come to bargain.")
  dormammu("You've come to die.",
    "Your world is now my world, like all worlds.")

bargain()
bargain()
dormammu("What is this?", "Illusion?")
strange("No, this is real.")

for i in range(0, randint(1,50)):
  bargain()
  dormammu("You cannot do this forever.")
  strange("Actually, I can.", "This is how things are now.",
    "You and me, trapped in this moment, endlessly.")
  dormammu("No! Make it stop.")

strange("Take your zealots from the Earth.",
  "End your assault on my world.", "Never come back.")
