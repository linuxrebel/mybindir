#!/usr/bin/python

#import the os and argparse modules
import os
import argparse
import time

# Create an argument parser object
parser = argparse.ArgumentParser(description="List files and directories in a given path")

# Add the arguments for the script
parser.add_argument("path", type=str, nargs="?", default=".", help="The path to list (default: current directory)")
parser.add_argument("-l", "--long", action="store_true", help="Show full information for each item")
parser.add_argument("-r", "--recursive", type=int, nargs="?", const=-1, default=1, help="Recurse through the directory tree, optionally specifying the depth (default: 1)")
parser.add_argument("-c", "--count", choices=["files", "dir", "all"], help="Show the count of files, directories, or both")


# Parse the arguments
args = parser.parse_args()

# Define a function to format the size of a file
def format_size(size):
    # Use binary prefixes (KiB, MiB, etc.)
    units = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
    # Find the appropriate unit for the size
    index = 0
    while size >= 1024 and index < len(units) - 1:
        size /= 1024
        index += 1
    # Return the size with two decimal places and the unit
    return f"{size:.2f} {units[index]}"

# Define a function to list the files and directories in a given path
def list_path(path, level=0, depth=-1):
    # Check if the path is valid
    if not os.path.exists(path):
        print(f"Invalid path: {path}")
        return
    # Check if the path is a file
    if os.path.isfile(path):
        print(f"{path}")
        return
    # Check if the recursion depth is reached
    if depth == 0:
        return
    # Get the list of files and directories in the path
    items = os.listdir(path)
    # Sort the items alphabetically
    items.sort()
    # Loop through the items
    for item in items:
        # Join the path and the item name
        full_path = os.path.join(path, item)
        # Check if the item is a directory
        if os.path.isdir(full_path):
            # Print the directory name with indentation
            print("  " * level + f"{item}")
            # Recursively list the subdirectory with increased level and decreased depth
            list_path(full_path, level + 1, depth - 1)
        else:
            # Print the file name with indentation
            print("  " * level + f"{item}")
            # Check if the long option is specified
            if args.long:
                # Get the size and the last modified time of the file
                size = os.path.getsize(full_path)
                mtime = os.path.getmtime(full_path)
                # Format the size and the time
                size = format_size(size)
                mtime = time.ctime(mtime)
                # Print the size and the time with indentation
                print("  " * (level + 1) + f"Size: {size}")
                print("  " * (level + 1) + f"Last modified: {mtime}")

# Define a function to count the files and directories in a given path
def count_path(path, option):
    # Initialize the counters for files and directories
    file_count = 0
    dir_count = 0
    # Check if the path is valid
    if not os.path.exists(path):
        print(f"Invalid path: {path}")
        return
    # Check if the path is a file
    if os.path.isfile(path):
        # Increment the file count
        file_count += 1
    # Check if the path is a directory
    if os.path.isdir(path):
        # Increment the directory count
        dir_count += 1
        # Recursively traverse the directory tree
        for root, dirs, files in os.walk(path):
            # Increment the file count by the number of files in the current directory
            file_count += len(files)
            # Increment the directory count by the number of subdirectories in the current directory
            dir_count += len(dirs)
    # Check the option and print the appropriate count
    if option == "files":
        print(f"Number of files: {file_count}")
    elif option == "dir":
        print(f"Number of directories: {dir_count}")
    elif option == "all":
        print(f"Number of files: {file_count}")
        print(f"Number of directories: {dir_count}")

# Check if the count option is specified
if args.count:
    # Call the count function with the path and the option
    count_path(args.path, args.count)
else:
    # Call the list function with the path and the recursion depth
    list_path(args.path, depth=args.recursive)

